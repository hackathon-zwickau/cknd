package de.lndc.cknd;

import javafx.application.Platform;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.util.Duration;

import java.io.File;

public class ViewController {

	private int threshold = -70;
	private int bands = 64;

	private double width;
	private double height;
	private double barWidth;

	private float[] dataPre;
	private float[] dataPost;

	private FileChooser fileChooser = new FileChooser();
	private MediaPlayer player;
	private boolean isPlaying;

	@FXML
	private Label filename;
	@FXML
	private Label artist;
	@FXML
	private Label title;
	@FXML
	private Button play;
	@FXML
	private Slider slider;
	@FXML
	private Canvas canvas;
	private GraphicsContext gc;

	private void initPlayer(File file) {
		if (player != null) {
			player.pause();
			player = null;
		}

		Media media = new Media(file.toURI().toString());
		player = new MediaPlayer(media);
		player.setOnReady(() -> {
			play.setDisable(false);

			Duration dur = player.getTotalDuration();
			slider.setMax(dur.toSeconds());
			slider.setDisable(false);

			height = canvas.getHeight();
			width = canvas.getWidth();
			barWidth = width / (bands - 1);

			gc = canvas.getGraphicsContext2D();
			gc.setFill(Color.WHITE);

			dataPre = new float[bands];
			dataPost = new float[bands];

			for (int i = 0; i < bands; i++) {
				dataPre[i] = 1;
				dataPost[i] = 1;
			}
		});
		player.setOnStopped(() -> {
			isPlaying = false;
			play.setText("Play");
			slider.setValue(0);
		});
		player.setOnEndOfMedia(() -> player.stop());
		player.setOnPlaying(() -> {
			play.setText("Pause");
			isPlaying = true;
		});
		player.setOnPaused(() -> {
			play.setText("Play");
			isPlaying = false;
		});

		player.setAudioSpectrumInterval(.01);
		player.setAudioSpectrumThreshold(threshold);
		player.setAudioSpectrumNumBands(1024);
		player.setAudioSpectrumListener((timestamp, duration, magnitudes, phases) -> {
			float[] correct = new float[bands];
			for (int i = 0; i < correct.length; i++) {
				correct[i] = magnitudes[i] - threshold + phases[i];
			}

//			correct = scaleUp(correct);
			correct = smooth(correct);
			prepareValues(correct);

			gc.clearRect(0, 0, width, height);
			for (int i = 0; i < dataPost.length; i++) {
				double barHeight = height - ((dataPost[i] * height) / Math.abs(threshold));
				if (barHeight >= height - 4) {
					barHeight = height - 4;
				}
				gc.fillRect(i * barWidth, barHeight, barWidth - (barWidth * .4), height);
			}
		});
		player.currentTimeProperty().addListener(observable -> Platform.runLater(() -> {
			Duration cur = player.getCurrentTime();
			slider.setValue(cur.toSeconds());
		}));

		slider.valueProperty().addListener(observable -> {
			if (slider.isValueChanging()) {
				player.seek(new Duration(slider.getValue() * 1000));
			}
		});

		String fileNameStr = file.getName();
		filename.setText(fileNameStr);

		artist.setText("");
		title.setText("");

		if (fileNameStr.contains(".mp3")) {
			fileNameStr = fileNameStr.replace(".mp3", "");
		}
		String tiles[] = fileNameStr.split("-", 2);

		String artistStr;
		String titleStr;
		artistStr = tiles[0].trim().toUpperCase();
		if (tiles.length > 1) {
			titleStr = tiles[1].trim().toUpperCase();
		} else {
			titleStr = "TITLE";
		}
		artist.setText(artistStr);
		title.setText(titleStr);
	}

	private void prepareValues(float[] array) {
		dataPre = array;
		for (int i = 0; i < bands; i++) {
			if (dataPre[i] >= dataPost[i]) {
				dataPost[i] = ((dataPre[i] - dataPost[i]) / 2) + dataPost[i];
			} else {
				dataPost[i] = ((dataPost[i] - dataPre[i]) / 2) + dataPre[i];
				if (dataPost[i] > 1) {
					dataPost[i] -= 1;
				}
			}
		}
	}

	@FXML
	public void buttonClicked(Event e) {
		String which = ((Button) e.getSource()).getId();
		switch (which) {
			case "load":
				try {
					initPlayer(fileChooser.showOpenDialog(null));
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				break;
			case "play":
				if (!isPlaying) {
					player.play();
				} else {
					player.pause();
				}
				break;
		}
	}

	private float[] scaleUp(float[] array) {
		float[] result = new float[array.length];
		for (int i = 0; i < array.length / 2; i++) {
			if (i == 0) {
				result[i] = array[i];
			} else {
				result[i * 2] = array[i];
				result[i * 2 - 1] = (array[i - 1] + array[i]) / 2;
			}
		}
		return result;
	}

	private float[] smooth(float[] array) {
		float[] newArr = new float[array.length];
		int smoothingPoints = 3;
		double sidePoints = Math.floor(smoothingPoints / 2);
		double cn = 1 / (2 * sidePoints + 1);
		for (int i = 0; i < sidePoints; i++) {
			newArr[i] = array[i];
			newArr[array.length - i - 1] = array[array.length - i - 1];
		}
		for (int i = (int) sidePoints; i < array.length - sidePoints; i++) {
			float sum = 0;
			for (int n = (int) -sidePoints; n <= sidePoints; n++) {
				sum += cn * array[i + n] + n;
			}
			newArr[i] = sum;
		}
		return newArr;
	}
}
